
module.exports = async () => {
	const qs = require('querystring')
	const axios = require('axios')
	console.log('[SYNC] Ejecución automatica iniciada.')
	console.log('[SYNC] CRON 0 * * * * (c/1 hora)')
	const estaciones =  await strapi.query('estaciones').find({},['id']);
	const headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
	}
	const url = "http://estaciones.ianigla.mendoza-conicet.gob.ar:8001/estaciones"
	var res = {
		actualizados: 0,
		error: 0
	}
	for (i = 0; i < estaciones.length; i++) {
		let TIPO = estaciones[i].Tipo
		let ultimoDato;
		if(TIPO == 'Ianiglia'){
			ultimoDato = await strapi.query('datos').find({ estacione:estaciones[i].id, _limit: 1, _sort: 'Fecha:desc' });
		}else{
			ultimoDato = await strapi.query('ianiglia').find({ estacion:estaciones[i].id, _limit: 1, _sort: 'Fecha:desc' });
		}
		let ultimaFecha = ultimoDato[0].Fecha
		let postData = qs.stringify({
			id: estaciones[i].Codigo,
			data: 'todos',
			ndata: 24,
		})
		let faux = await axios.post(url, postData,{headers: headers})
		let aux = faux.data
		for (y = 0; y < aux.length; y++) {
			let ultima = new Date(ultimaFecha)
			let actual = new Date(aux[y].date)
			if(ultima.getTime() < actual.getTime()){
				if(TIPO == 'Ianiglia'){
					try {
						let auxRecord = await strapi.query('datos').create({
							estacione: estaciones[i].id,
							Fecha: aux[y].date,
							YJday: aux[y].YJday,
							Hora: aux[y]['hh.mm.ss'],
							batVolts: aux[y]['bat.Volts'],
							Temp_Aire: aux[y].Temp_Aire,
							H_relativa: aux[y].H_relativa,
							P_atm: aux[y].P_atm,
							Precip_Total: aux[y].Precip_Total,
							Vel_Viento: aux[y].Vel_Viento,
							Dir_Viento: aux[y].Dir_Viento,
							Irradiancia: aux[y].Irradiancia,
							Alt_Nieve: aux[y].Alt_Nieve,
							T_Suelo: aux[y].T_suelo,
							created_by: 1,
						})
						res.actualizados = res.actualizados + 1 
					} catch(e) {
						res.error = res.error + 1
						console.log(e);
					}
				}else{
					try {
						let auxRecord = await strapi.query('ianiglia').create({
							estacion: estaciones[i].id,
							AN_Max: aux[y].AN_Max,
							AN_Min:aux[y].AN_Min,
							AN_Prom: aux[y].AN_Prom,
							EAN_Max: aux[y].EAN_Max,
							EAN_Min: aux[y].EAN_Min,
							EAN_Prom: aux[y].EAN_Prom,
							Pres_Max: aux[y].Pres_Max,
							Pres_Min: aux[y].Pres_Min,
							Pres_Prom: aux[y].Pres_Prom,
							Temp_Max: aux[y].Temp_Max,
							Temp_Min: aux[y].Temp_Minin,
							Temp_Prom: aux[y].Temp_Prom,
							HR_Max: aux[y].HR_Max,
							HR_Min: aux[y].HR_Min,
							HR_Prom: aux[y].HR_Prom,
							Tsuelo_Max: aux[y].Tsuelo_Max,
							Tsuelo_Min: aux[y].Tsuelo_Min,
							Tsuelo_Prom: aux[y].Tsuelo_Prom,
							RadI_Max: aux[y].RadI_Max,
							RadI_Min: aux[y].RadI_Min,
							RadI_Prom: aux[y].RadI_Prom,
							RadR_Max: aux[y].RadR_Max,
							RadR_Min: aux[y].RadR_Min,
							RadR_Prom: aux[y].RadR_Prom,
							DirViento_Max: aux[y].DirViento_Max,
							DirViento_Prom: aux[y].DirViento_Prom,
							VelViento_Max: aux[y].VelViento_Max,
							VelViento_Min: aux[y].VelViento_Min,
							VelViento_Prom: aux[y].VelViento_Prom,
						})
						res.actualizados = res.actualizados + 1
					} catch(e) {
						res.error = res.error + 1
						console.log(e);
					}
				}
			}
		}			
	}
	console.log(res)
	return res
};