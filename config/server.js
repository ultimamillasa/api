module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  cron: { enabled: true },
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '45c071ec6ec4867c30bc3930de0faf7d'),
    },
  },
});
