'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

 module.exports = {
 	async charts(ctx) {
 		const params = ctx.params
 		var min = Number.POSITIVE_INFINITY;
 		var max = Number.NEGATIVE_INFINITY;
 		
 		const result = await strapi
 		.query('datos')
 		.model.query(qb => {
 			qb.where('estacione', params.estacion);
 			qb.where("Fecha", '>=' , params.desde);
 			qb.where("Fecha", '<=' , params.hasta);
 		})
 		.fetchAll();
 		var groupArrays = '';
 		var fields = result.toJSON();
 		if(params.frecuencia != 'Horaria' ){

 			const groups = fields.reduce((groups, game) => {
 				var date = ''
 				if(params.frecuencia == 'Diaria'){
 					date = game.Fecha.split('T')[0];
 				}else if(params.frecuencia == 'Mensual'){
 					date = game.Fecha.split(('-'))[0]+"-"+game.Fecha.split(('-'))[1]
 				}
 				if (!groups[date]) {
 					groups[date] = [];
 				}
 				groups[date].push(game);
 				return groups;
 			}, {});
 			groupArrays = Object.keys(groups).map((date) => {
 				return {
 					date,
 					games: groups[date]
 				};
 			});
 		}
 		var r = {
 			data: [],
 			labels: []
 		}
 		groupArrays.forEach(function(t){
 			r.labels.push(t.date)
 			var acum = 0
 			t.games.forEach(function(g){
 				var cifra = g[params.variable]
 				if(params.tipo == 'Maximo'){
 					if (cifra > max) max = cifra;
 				}else if(params.tipo == 'Minimo'){
 					if (cifra < min) min = cifra;
 				}else if(params.tipo == 'Promedio'){
 					acum = acum + cifra
 				}else if(params.tipo == 'Acumulado'){ //Acumulado
 					acum = acum + cifra
 				}
 			})
 			if(params.tipo == 'Maximo'){
 				r.data.push(max.toFixed(2))
 			}
 			if(params.tipo == 'Minimo'){
 				r.data.push(min.toFixed(2))
 			}
 			if(params.tipo == 'Promedio'){
 				r.data.push((acum/t.games.length).toFixed(2))
 			}
 			if(params.tipo == 'Acumulado'){
 				r.data.push(acum.toFixed(2))
 			}
 		})


 		//const datos = await strapi.query('datos').find({ estacione: params.estacion });
 		ctx.send(r);
 	}
 };
